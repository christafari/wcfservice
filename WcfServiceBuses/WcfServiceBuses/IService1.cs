﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceBuses
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {


  

         [OperationContract]
         [WebInvoke( Method = "GET", ResponseFormat = WebMessageFormat.Json,
              BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate= "Existe/{cedula}"
             
             )]
         String Existe(String cedula);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        String retornarbuses(string co, string cd);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        String consultar_reservas( int id);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        String Agregar_reserva(int cedula, int viaje, int puesto, DateTime fecha);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        String eliminar_reserva(int id_reserva);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        String Agregar_usuario(string ced, string no, string apell, string dir, string tel, string em);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        String disponibilidad_puestos(String id_viaje, String id_bus);


        // TODO: agregue aquí sus operaciones de servicio
    }



    

    // Utilice un contrato de datos, como se ilustra en el ejemplo siguiente, para agregar tipos compuestos a las operaciones de servicio.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
