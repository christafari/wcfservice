﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfServiceBuses
{
    public class reserva
    {

        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        int puesto;

        public int Puesto
        {
            get { return puesto; }
            set { puesto = value; }
        }

        string ciudad_origen = string.Empty;

        public string Ciudad_origen
        {
            get { return ciudad_origen; }
            set { ciudad_origen = value; }
        }

        string ciudad_destino = string.Empty;

        public string Ciudad_destino
        {
            get { return ciudad_destino; }
            set { ciudad_destino = value; }
        }
        string fecha;

        public string Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

    }

    public class reservas_arr
    {
        List<reserva> lista;

        public List<reserva> Lista
        {
            get { return lista; }
            set { lista = value; }
        }
    }

}