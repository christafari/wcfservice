﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;

namespace WcfServiceBuses
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    
    public class Service1 : IService1
    {
        BusesEntities bus = new BusesEntities();

        public String Existe(String Cedula)
        {
            int ce = Int32.Parse(Cedula);
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            String JSON = string.Empty;
            var login = from usuario in bus.Usuario
                        where usuario.Cedula == ce
                        select new
                        {
                            usuario
                        };

            Usuario loggin = null ;

            foreach (var user in login)
            {
                loggin = new Usuario();
                loggin.Cedula = user.usuario.Cedula;
                loggin.Nombres = user.usuario.Nombres;
                loggin.Apellidos = user.usuario.Apellidos;
                loggin.Direccion = user.usuario.Direccion;
                loggin.Email = user.usuario.Email;
                loggin.Telefono = user.usuario.Telefono;
            }
            if(loggin != null){

                JSON =serializador.Serialize(loggin);
                return JSON;
            }
            else
            {
                return "no existe el usuario";
            }
            
        }


        public String retornarbuses(string co,string cd){

            JavaScriptSerializer serializador = new JavaScriptSerializer();
            String JSON = string.Empty;
            

            var bu = from viaje in bus.Viaje
                     join buses in bus.Bus
                     on viaje.id_bus equals buses.Id_bus
                     join agenda in bus.Agenda
                     on viaje.id_agenda equals agenda.id_agenda
                     where  viaje.Ciudad_origen==co && viaje.Ciudad_destino == cd 
                     select new {
                         id_viaje =viaje.id_viaje,
                         id_bus=viaje.id_bus,
                         capacidad =buses.capacidad,
                         tipo=buses.tipo,
                         hora_partida=agenda.Hora_partida,
                         fecha = agenda.Fecha
                     };

            List<buseslista> Disponibles = new List<buseslista>();
            foreach (var usuario in bu)
            {      
                    buseslista a = new buseslista();
                    a.Id_viaje = usuario.id_viaje;
                    a.Id_bus=usuario.id_bus;
                    a.Capacidad= usuario.capacidad;
                    a.Tipo =usuario.tipo;
                    a.Hora_partida = usuario.hora_partida.ToString();
                    a.Fecha = usuario.fecha.ToString("dd/MM/yyyy");
                    Disponibles.Add(a);

                   
            }
           if(Disponibles.Count()==0){
               JSON = serializador.Serialize("No hay disponibilidad");
           }
           else
           {
               buses_ar arr = new buses_ar();
               arr.Lista = Disponibles;
               JSON = serializador.Serialize(arr);
           }
           
                return JSON;
        }



        public String consultar_reservas(int id_usuario){

            JavaScriptSerializer serializador = new JavaScriptSerializer();
            String JSON = string.Empty;
            var registro = from reserva   in bus.Reservas
                           join viaje in bus.Viaje
                           on reserva.id_viaje equals viaje.id_viaje
                           where reserva.cedula_usuario == id_usuario
                           select new {
                           origen =viaje.Ciudad_origen,
                           destino = viaje.Ciudad_destino,
                           id_reserva = reserva.id_reserva,
                           puesto = reserva.puesto,
                           fecha = reserva.fecha_reserva
                           };

            List<reserva> reservas = new List<reserva>();

            foreach (var reserva in registro)
              {
                  reserva a = new reserva();
                    a.Id=reserva.id_reserva;
                    a.Puesto = reserva.puesto; 
                    a.Ciudad_origen=reserva.origen;
                    a.Ciudad_destino=reserva.destino;
                    a.Fecha = reserva.fecha.ToString("dd/MM/yyyy");
                reservas.Add(a);

                }
            if (reservas.Count() == 0)
            {
                string err = "No hay reservas";
                JSON = serializador.Serialize(err);
            }
            else
            {
                reservas_arr reser = new reservas_arr();
                reser.Lista = reservas;
                JSON = serializador.Serialize(reser);
            }

            
                return JSON;      
        }

        public string Agregar_reserva(int cedula,int viaje ,int puesto,DateTime fecha)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            String JSON = string.Empty;
            var reserva = new Reservas ();
            reserva.cedula_usuario = cedula;
            reserva.id_viaje = viaje;
            reserva.puesto = puesto;
            reserva.fecha_reserva = fecha;
            bus.Entry(reserva).State = reserva.id_reserva == 0 ?
                EntityState.Added :
                EntityState.Modified;
           /* bus.Reservas.Add(reserva);
            bus.SaveChanges();*/
            bus.SaveChanges();
            JSON = ser.Serialize(reserva.id_reserva);
            return JSON;
        }

        public string Agregar_usuario(string cedula,string no,string apell,string dir,string tel,string em)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            string JSON = string.Empty;
            Usuario a = new Usuario();
            a.Cedula = Int32.Parse(cedula);
            a.Nombres = no;
            a.Apellidos = apell;
            a.Telefono = Int32.Parse(tel);
            a.Direccion = dir;
            a.Email = em;
            bus.Usuario.Add(a);
            bus.SaveChanges();
            JSON = ser.Serialize(a.Cedula);

            return JSON;
        }

        public String eliminar_reserva(int id_reserva)
        {
            String estado="";
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            String JSON = string.Empty;
            var reserva = bus.Reservas.Where(p => p.id_reserva==id_reserva).First();
            if(reserva != null){
                bus.Reservas.Remove(reserva);
                bus.SaveChanges();
                estado="Eliminacion Correcta";
                JSON = serializador.Serialize(estado);
                return JSON;
            }
            return "Se ha producido un error" ;
        }

        public String disponibilidad_puestos(String id_viaje,String id_bus)
        {
            int id= Int32.Parse(id_bus);
            int id_viaj = Int32.Parse(id_viaje);
            JavaScriptSerializer ser = new JavaScriptSerializer();
            String JSON = string.Empty;
            List<estado> puestos = new List<estado>();

            var capacidad = from buses in bus.Bus
                            where buses.Id_bus == id
                            select new
                            {
                                capacidad= buses.capacidad
                            };

            var ocupados = from viaje in bus.Viaje
                           join reserv in bus.Reservas
                           on viaje.id_viaje equals reserv.id_viaje
                           where viaje.id_viaje == id_viaj
                           select new
                           {
                               puesto =reserv.puesto
                           };

            int cantidad = 0;
            foreach (var it in capacidad)
            {
                cantidad = it.capacidad;
            }
            estado[] disponibilidad = new estado[cantidad];
            int cont=0;

            for (int i = 0; i < disponibilidad.Length; i++)
            {
                estado a = new estado();
                a.Estado="Disponible";
                disponibilidad[i]=a;
            }

            foreach (var item in ocupados)
            {
               cont+=1;
               estado a = new estado();
               a.Estado="Ocupado";
               disponibilidad[item.puesto - 1] = a;
            }
            listapuesto p = new listapuesto();
            p.Estados = disponibilidad;

            JSON = ser.Serialize(p);
            
              

            return JSON;
        }









    }
}
